package com.example.demo.batch;

import com.example.demo.model.User;
import com.example.demo.repo.UserRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DBWriter implements ItemWriter<User> {

    @Autowired
    private UserRepository repository;


    @Override
    public void write(List<? extends User> users) throws Exception {
        System.out.println("Data Saved for user:"+ users);
        repository.saveAll(users);
    }
}

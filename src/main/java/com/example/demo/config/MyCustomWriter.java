package com.example.demo.config;

import com.example.demo.model.User;
import org.springframework.stereotype.Component;

import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.core.io.FileSystemResource;


@Component
public class MyCustomWriter extends FlatFileItemWriter<User> {

    public MyCustomWriter() {
        setResource(new FileSystemResource("output.csv"));
        setLineAggregator(getDelimitedLineAggregator());
    }

    public DelimitedLineAggregator<User> getDelimitedLineAggregator() {
        BeanWrapperFieldExtractor<User> beanWrapperFieldExtractor = new BeanWrapperFieldExtractor<User>();
        beanWrapperFieldExtractor.setNames(new String[]{"id", "name"});

        DelimitedLineAggregator<User> delimitedLineAggregator = new DelimitedLineAggregator<User>();
        delimitedLineAggregator.setDelimiter(",");
        delimitedLineAggregator.setFieldExtractor(beanWrapperFieldExtractor);
        return delimitedLineAggregator;

    }
}